#!/usr/bin/env python
__doc__ = "To plot weighted gamma gamma spectrum"

import sys
import yaml
import ROOT
try:
  import AtlasStyle
except ImportError:
  ROOT.gROOT.LoadMacro("AtlasStyle.C")
  ROOT.SetAtlasStyle()

ROOT.gROOT.SetBatch(ROOT.kTRUE)

from ROOT import RooFit
Import = getattr(ROOT.RooWorkspace, 'import')

#cfile = 'config/catinfo.yml'
#if (len(sys.argv) > 1): cfile = sys.argv[1]
if (len(sys.argv) <= 1):
  print 'Usage: ./weightedPlot path/to/config.yml'
  sys.exit()
config = yaml.safe_load(open(sys.argv[1]))

def test():
  "For testing: return dictionary to be used directly with getWeightedWorkspace"
  w = ROOT.RooWorkspace('w')
  eG = w.factory('ExtendPdf::eG(Gaussian::G(x[-5,5], 0, 1), nG[100])')
  eU = w.factory('ExtendPdf::eU(Uniform::U(y[-5,5]), 1000)')
  pdf = w.factory('SIMUL::pdf( cat[a,b], a=eG, b=eU )')

  w.defineSet('aset', 'cat,x,y')
  dataG = eG.generate(ROOT.RooArgSet(w.obj('x')))
  dataU = eU.generate(ROOT.RooArgSet(w.obj('y')))
  data = ROOT.RooDataSet("combData","combined data", w.set('aset'),
    RooFit.Index(w.obj('cat')),RooFit.Import("a", dataG),RooFit.Import("b",dataU))

  catInfo = dict(
    a = dict( weight = 0.2, pdfName = 'eG', observable = 'x' ),
    b = dict( weight = 0.1, pdfName = 'eU', observable = 'y' )
  )
  return dict(w=w, data=data, catVariable='cat', catInfo=catInfo, newObs='mgg', nBins=55)

def getInfoCouplings(fName='WS-HGam-STXSMerged.root', snapshot='ucmles', noWeights = False, nBins=55, subset=None):
  """getInfoCouplings(fName='WS-HGam-Coupling.root', snapshot='ucmles_prod5', noWeights = False, subset=None) ->
   return info for getWeightedWorkspace. <subset> can be ggH, VBF, VH or ttH"""
  f = ROOT.TFile(fName)
  w = f.combWS
  if snapshot: w.loadSnapshot(snapshot)

  catInfo = config['CatInfo']

  if subset:
    catInfo = dict( (key, value) for key, value in catInfo.iteritems() if subset in key)

  if noWeights: # Set all weights to 1
    for d in catInfo.values(): d['weight'] = 1

  return dict(w=w, catInfo=catInfo, data = w.obj('combData'), catVariable = 'channellist', nBins = nBins, newObs = 'mgg')


def getLegend(frame, labels, position=[0.57, 0.63, 0.87, 0.87], plot_options={}, add=True):
  """getLegend(frame, labels, position=[0.24, 0.2, 0.55, 0.45], plot_options={}, add=True) -->
  Create and return a legend. <plot_options> is a dictionary with the plot option
  for each object ('PL' for data, 'L' otherwise if not given)
  """
  legend = ROOT.TLegend(*position)
  legend.SetBorderSize(0)
  legend.SetFillColor(0)
  legend.SetTextFont(42)
  legend.SetTextSize(0.050)

  for i, label in enumerate(labels):
    obj = frame.getObject(i)
    # Set default plot options
    if isinstance(obj, (ROOT.RooAbsData, ROOT.RooHist)):
      plot_options.setdefault(obj, 'PE')
    else:
      plot_options.setdefault(obj, 'L')
    # Add entry
    legend.AddEntry(obj, label, plot_options[obj])

  if add:
    frame.addObject(legend)
  return legend


def getNewWorkspace(w, data, catVariable, catInfo, name='w1', newObs='mgg', nBins=55,
  dataName='dataW', pdfName='pdfW', newWorkspace=None, addData = True):
  """getNewWorkspace(w, data, catVariable, catInfo, newObs='mgg', nBins=55) -->
  return a workspace with dataName and pdfName that represent the weighted sum
  of data and pdfs in the categories (catVariable).
  catInfo is a dictionary (catName, info) like:
    catInfo = dict(
      a = dict( weight = 0.2, pdfName = 'eG', observable = 'x' ),
      b = dict( weight = 0.1, pdfName = 'eU', observable = 'y' )
    )
  """
  # Create new workspace and observable, using the overlapping range of all observables
  w1 = newWorkspace or ROOT.RooWorkspace(name)
  if not w1.allVars().find(newObs):
    obs = w1.factory('{0}[{1}, {2}]'.format(
      newObs,
      max(w.obj(d['observable']).getMin() for d in catInfo.values() ),
      min(w.obj(d['observable']).getMax() for d in catInfo.values() )
    ) )
    obs.setBins(nBins)

  # Change observable name and create combined dataHist
  if addData:
    h = ROOT.TH1F('h', '', nBins, obs.getMin(), obs.getMax())
    hAll = h.Clone('hAll')
    hAll.Sumw2()
    for catName, d in catInfo.iteritems():
      dcat = data.reduce('{0} == {0}::{1}'.format(catVariable, catName) )
      alist = ROOT.RooArgList( data.get().selectByName(d['observable']) )
      _ = dcat.fillHistogram(h, alist)
      _ = hAll.Add(h, d['weight'])
      h.Reset()

    dataW = ROOT.RooDataHist(dataName, dataName, ROOT.RooArgList(obs), hAll)
    Import(w1, dataW)

  # Add expected events to catInfo and
  # import pdf of each category, changing the name of the observable
  for catName, d in catInfo.iteritems():
    pdfCat = w.obj(d['pdfName'])
    d['norm'] = pdfCat.expectedEvents( data.get() )
    Import(w1, pdfCat, RooFit.RenameVariable(d['observable'], newObs), RooFit.RecycleConflictNodes() )

  # Create sum of pdfs
  _ = ', '.join('{w} * {pdf}'.format(w=d['weight']*d['norm'], pdf=d['pdfName']) for d in catInfo.values())
  pdfW = w1.factory('SUM::{0}( {1} )'.format(pdfName, _) )
  return w1


def plot(w1, label='Couplings Categories', canvasName='c', canvasTitle='c'):

  c = ROOT.TCanvas(canvasName, canvasTitle, 100, 10, 800, 800)
  c.mem = []

  r = 0.35
  epsilon = 0.02

  pad_bottom = ROOT.TPad("pad_bottom", "pad_bottom", 0, 0., 1, r * (1 - epsilon))
  pad_bottom.SetFrameFillStyle(4000)
  pad_top = ROOT.TPad("pad_top", "pad_top", 0, r - epsilon, 1, 1)
  pad_top.SetFrameFillStyle(4000)
  c.mem.append(pad_bottom)
  c.mem.append(pad_top)
  pad_top.SetMargin(0.15, 0.03, epsilon, 0.04)  # l r b t
  pad_bottom.SetMargin(0.15, 0.03, 0.3, 0.0)
  pad_top.SetBottomMargin(epsilon)
  pad_bottom.SetTopMargin(0)
  pad_bottom.SetFillColor(0)
  pad_bottom.SetFrameFillStyle(0)
  pad_bottom.SetFillStyle(0)
  pad_top.SetFillColor(0)
  pad_top.SetFrameFillStyle(0)
  pad_top.SetFillStyle(0)

  c.cd()
  pad_bottom.Draw()
  c.cd()
  pad_top.Draw()
  pad_top.cd()

  mgg = w1.obj('mgg')
  binWidth = mgg.getBinWidth(0)

  frame = mgg.frame()
  w1.obj('dataW').plotOn(frame, RooFit.XErrorSize(0))
  w1.var('mu').setVal(0.0)
  if not w1.allPdfs().find('pdfB'):
    pdfB = w1.obj('pdfW')
  else:
    pdfB = w1.obj('pdfB')
  pdfB.plotOn(frame, RooFit.LineStyle(2), RooFit.LineColor(ROOT.kBlue), RooFit.Normalization(1, ROOT.RooAbsReal.RelativeExpected))
  residHist = frame.residHist()
  pullHist = frame.pullHist()

  NBg = pdfB.expectedEvents( w1.obj('dataW').get() )

  w1.var('mu').setVal(1.0)
  w1.obj('pdfW').plotOn(frame, RooFit.LineStyle(1), RooFit.LineColor(ROOT.kRed), RooFit.Normalization(1, ROOT.RooAbsReal.RelativeExpected))
  frame.SetMaximum(frame.GetMaximum()*1.35)

  NSigPlusBg = w1.obj('pdfW').expectedEvents( w1.obj('dataW').get() )

  NSg = NSigPlusBg - NBg
  w1.factory('signalOnlyYield[1.0]')
  w1.var('signalOnlyYield').setVal(NSg)

  #w1.var('normNF_dataSB').setVal(0.0)
  w1.var('NFcontBkg').setVal(0.0) # if this looks funny need a SF on spursig
  w1.obj('pdfW').plotOn(frame, RooFit.LineStyle(1), RooFit.LineColor(ROOT.kBlack), RooFit.Normalization( NSg,ROOT.RooAbsReal.NumEvent))

  # Add legend
  legend = getLegend(frame, ['Data', 'Background', 'Signal + Background', 'Signal'], [0.18, 0.71, 0.51, 0.92])
  #legend = getLegend(frame, ['Data', 'Background', 'Signal + Background', 'Signal'], [0.21, 0.75, 0.53, 0.93])

  frame.SetMinimum(1E-4)
  frame.Draw()
  c.mem.append(frame)

  offset_y = 1.0
  frame.GetYaxis().SetLabelSize(0.041)
  frame.GetYaxis().SetTitleSize(0.043)
  frame.GetYaxis().SetTitleOffset(1.21 * offset_y)
  frame.SetLabelSize(0.)
  frame.SetTitleSize(0.)

  #frame.GetYaxis().SetTitle("Sum of weights / %s" % ('GeV' if binWidth==1 else ' %s GeV' % binWidth))
  frame.GetYaxis().SetTitle("#scale[0.8]{#sum} weights / %s" % ('GeV' if binWidth==1 else ' %s GeV' % binWidth))
  frame.GetXaxis().SetTitle("m_{#gamma#gamma} [GeV]")
  #frame.GetYaxis().SetTitleSize(0.33)
  #frame.GetYaxis().SetNdivisions(508)

  # Draw Labels
  #ROOT.ATLASLabel(0.69,0.876,"Internal")
  tl = ROOT.TLatex()
  tl.SetNDC()
  tl.SetTextFont(42)
  tl.SetTextSize(0.064)
  tl.DrawLatex(0.64, 0.875, "#bf{#it{ATLAS}} Internal")
  #tl.DrawLatex(0.60, 0.875, "#bf{#it{ATLAS}} Preliminary")
  tl.SetTextSize(0.050)
  tl.DrawLatex(0.64,0.820,"#sqrt{s} = 13 TeV, 36.1 fb^{-1}")
  tl.DrawLatex(0.66,0.770,"m_{H} = 125.09 GeV")
  tl.SetTextSize(0.050)
  if 'WeightLabel' in config:
    tl.DrawLatex(0.58,0.70,config['WeightLabel'])
  else:
    tl.DrawLatex(0.58,0.70,"ln(1+S/B) weighted sum")
  tl.DrawLatex(0.58,0.64,label)

  pad_bottom.cd()

  frame2 = mgg.frame()
  w1.obj('pdfW').plotOn(frame2, RooFit.LineStyle(1), RooFit.LineColor(ROOT.kBlack), RooFit.Normalization( NSg,ROOT.RooAbsReal.NumEvent))

  frame2.GetXaxis().SetTitle(r"#it{m}_{#gamma#gamma} [GeV]")
  frame2.GetXaxis().SetTitleSize(0.10)
  frame2.GetXaxis().SetTitleOffset(1.25)
  frame2.GetXaxis().SetLabelSize(0.081)
  frame2.GetYaxis().SetLabelSize(0.077)
  frame2.GetYaxis().SetTitleSize(0.077)
  frame2.GetYaxis().SetTitleFont(42)
  frame2.GetYaxis().SetTitleOffset(0.66 * offset_y)
  frame2.GetYaxis().CenterTitle(True)

  #frame2.GetYaxis().SetTitle("data - background")
  frame2.GetYaxis().SetTitle("#scale[0.8]{#sum} weights - fitted bkg")
  frame2.GetYaxis().SetNdivisions(508)
  frame2.addPlotable(residHist, "P");
  frame2.Draw()
  ROOT.gPad.Update()
  return c, frame, legend, tl, residHist, pullHist


if __name__ == '__main__':
  filename = config['InputFile']
  print 'Running over filename:', filename
  info = getInfoCouplings(fName=filename, nBins=55)
  w = getNewWorkspace(**info)
  info['w'].obj('mu').setVal(0)
  getNewWorkspace(newWorkspace=w, addData=False, pdfName='pdfB', **info)
  label = '%s' % config['Label']
  x = plot(w,label=label)

  x[0].SaveAs(config['OutputFile'])
  print 'Plot written to', config['OutputFile']
  import sys; sys.exit()


